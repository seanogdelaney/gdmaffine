import numpy as np

# First define 3 pairs of integer ilxl coordinates
# I'll assume they prescribe a rectangle aligned with the grid.
# I'll also assume they're in this order. Logic required in general.
il00 = 1
xl00 = 1
il01 = 1
xl01 = 1001
il10 = 1001
xl10 = 1
# Corresponding real (double precision) x,y coordinates
# For simplicity, angle = 0, origin (1,1) <=> (0.0, 0.0), scale = 10.0 x 10.0
x00 = 10.0
y00 = 10.0
x01 = 10.0
y01 = 10010.0
x10 = 10010.0
y10 = 10.0

# Find two orthogonal vectors from the points
ilVec = np.array([x10 - x00, y10 - y00]) / (il10 - il00)
xlVec = np.array([x01 - x00, y01 - y00]) / (xl01 - xl00)

# Now the user chooses another point on the integer grid
ilUser = 201
xlUser = 301

# We find the relative grid position from a point we know
ilRel = ilUser - il00
xlRel = xlUser - xl00

# We use the relative index, and the point we know, to find the result
xUser = x00 + ilVec[0] * ilRel + xlVec[0] * xlRel
yUser = y00 + ilVec[1] * ilRel + xlVec[1] * xlRel

print(xUser, yUser)

# Converting back to integer grid
vecUser = np.array([xUser - x00, yUser - y00])
ilUser = il00 + int(round(np.dot(vecUser, ilVec) / np.sum(ilVec**2)))
xlUser = xl00 + int(round(np.dot(vecUser, xlVec) / np.sum(xlVec**2)))

print(ilUser, xlUser)
